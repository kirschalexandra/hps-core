;; Copyright 2017
;;
;; Licensed under the Apache License, Version 2.0 (the "License");
;; you may not use this file except in compliance with the License.
;; You may obtain a copy of the License at
;;
;; http://www.apache.org/licenses/LICENSE-2.0
;;
;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an "AS IS" BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.

(ns hps-test.core
  (:use clojure.test)
  (:use hps-test.examples.food)
  (:use hps.types)
  (:use hps.make-decision)
  (:use hps.decision-modules))

;(defn close?
;  ([x y] (close? x y 0.001))
;  ([x y tolerance]
;   (< (Math/abs (- x y)) tolerance)))

(def food-dm (get-dm-from-name :food))
(def setup (configure-decision-module food-dm (:decision-parameters food-dm)))

(deftest test-produce
  (is (= (:value (first (produce-alternatives (first (:producers (:experts setup))))) 'pizza)))
  (is (= (count (produce-alternatives (second (:producers (:experts setup))))) 2)))

(deftest test-evaluate
  (let [alternatives (mapcat hps.make-decision/produce-alternatives (:producers (:experts setup)))]
    (is (= (count alternatives) 3))))

(deftest test-get-and-evaluate
  (let [res (hps.make-decision/get-and-evaluate-alternatives setup)]
    (is (= (count res) 3))))



    
