; Copyright 2017
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
; http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

(ns hps.types)

(defrecord DecisionAlternative [value produced-by altered-by evaluations aggregated-evaluation])
(defrecord DecisionResult [decision retained-alternatives rejected-alternatives])
(defrecord DecisionSetup [experts alternatives parameters])

; hier neue Definition von Experten
(defrecord Expert [name type fun parameters])               ; parameters: die Parameter für get-and-evaluate-alternatives

(defrecord DecisionModule
  [;; 1) zu spezifizierende/ konfigurierbare Slots
   name
   goaltype                                                 ; wenn DM Ziele hat, dann hier einen Goaltype (s.u.) reinschreiben
   available-experts
   decision-parameters                                      ; default: hps.decision-modules/default-decision-params
   ;; Funktionen, die das setup genieren:
   ;; sowohl configure als auch reconfigure bekommen das DM übergeben
   ;; die Idee ist eigentlich, dass configure in erster Linie aufgrund des Zustands entscheidet, während
   ;; reconfigure auch die letzte Entscheidung und/ oder das letzte Setup einbezieht
   ;; aber alle diese Angaben stehen im DM in den jeweiligen Atomen und können von beiden Funktionen nach Belieben verwendet werden
   configure-fun                                            ; default: alle available-e
   reconfigure-fun                                          ; default: configure-fun
   ; benötigte andere decision modules (nur für top-level-dm's)
   dm-dependencies
   ;; 2) intern genutzte Slots, die automatisch ergänzt werden
   ; "Interface" durch Atome
   goal decision-setup internal-decision executed-decision
   decision-process                                         ; future, in dem die Endlosschleife von run-decision-process läuft
   ; slot zum eleganteren Beenden des DM
   active
   ; reinfummeln von Code zur Alternativen-Generierung (Connections zwischen DMs)
   decision-to-alternatives-connections-code
   ])

(defrecord Goaltype [name statevars closeness-function threshold parameters])
(defrecord Goal [type timeout parent-goal])


