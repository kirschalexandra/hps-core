# Use HPS as a local library

1. Create a new leiningen project
2. Inside the root folder of that project create a directory 'checkouts'
3. Inside the 'checkouts' folder create a symlink to the root directory of HPS (this directory):
   ```ln -s \path\to\hps-core hps```
4. Add '''[hps "1.0.0-SNAPSHOT"]'''  to the ':dependencies' section in the projects 'project.clj'
5. Run ```lein install``` once in this directory
5. Done.


# Pitfalls with local dependencies
1. Calling '''lein clean''' in a depending project must be followed by '''lein compile''' in this package
